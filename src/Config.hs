{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeApplications #-}
module Config where

import           Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import qualified Data.ByteString.UTF8 as BS
import qualified Data.ByteString.Lazy as LBS
import Blaze.ByteString.Builder (fromByteString, fromLazyByteString)
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Lazy  as Text (toStrict)
import qualified Conferer as C
import qualified Conferer.Types as C
import Conferer.FetchFromConfig.Warp ()
import Conferer.FetchFromConfig.Hedis ()
import GHC.Generics
import           Network.Wai.Handler.Warp as Warp
import qualified Database.Redis as Redis

instance Show Settings where
  show _ = "Settings {}"

data AppConfig = AppConfig
  { appConfigRootsOnly :: Bool
  , appConfigSessionDurationSeconds :: Integer
  , appConfigBackoffice :: BackofficeConfig
  , appConfigProxy :: ProxyConfig
  , appConfigServer :: Settings
  , appConfigRedis :: Redis.ConnectInfo
  } deriving (Generic, Show)

instance C.DefaultConfig AppConfig where
  configDef = AppConfig
    { appConfigRootsOnly = False
    , appConfigSessionDurationSeconds = 60 * 60 * 24
    , appConfigBackoffice = C.configDef
    , appConfigProxy = C.configDef
    , appConfigServer = setPort 3334 defaultSettings
    , appConfigRedis = C.configDef
    }
instance C.UpdateFromConfig AppConfig
instance C.FetchFromConfig AppConfig

data ProxyConfig = ProxyConfig 
  { proxyConfigPort :: Int
  , proxyConfigHost :: ByteString
  } deriving (Generic, Show)

instance C.DefaultConfig ProxyConfig where
  configDef = ProxyConfig 
    { proxyConfigPort = 4567
    , proxyConfigHost = "127.0.0.1"
    }
instance C.UpdateFromConfig ProxyConfig
instance C.FetchFromConfig ProxyConfig

data BackofficeConfig = BackofficeConfig
  { backofficeConfigHost :: Text
  , backofficeConfigAppId :: Text
  , backofficeConfigAppSecret :: Text
  } deriving (Generic, Show)

instance C.DefaultConfig BackofficeConfig where
  configDef = BackofficeConfig
    { backofficeConfigHost = "backoffice.10pines.com"
    , backofficeConfigAppId = error "App id para el backoffice es requerido"
    , backofficeConfigAppSecret = error "App secret para el backoffice es requerido"
    }
instance C.UpdateFromConfig BackofficeConfig
instance C.FetchFromConfig BackofficeConfig
