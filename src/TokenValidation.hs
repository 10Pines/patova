{-# LANGUAGE DeriveGeneric #-}

module TokenValidation (getUserFromToken, payloadEsRoot) where

import GHC.Generics
import qualified Network.HTTP.Client as Client
import           Network.HTTP.Types
import           Network.HTTP.Types.Header (hCookie, hSetCookie)
import           Network.Wai
import           Network.Wai.Handler.Warp as Warp
import Control.Monad
import           Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import qualified Data.ByteString.UTF8 as BS
import qualified Data.ByteString.Lazy as LBS
import Blaze.ByteString.Builder (fromByteString, fromLazyByteString)
import Network.HTTP.ReverseProxy
import Web.Cookie
import Data.String.Conversions
import Data.Binary.Builder (toLazyByteString)
import Data.List (find)
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Lazy  as Text (toStrict)
import qualified Crypto.MAC.HMAC as Crypto
import qualified Crypto.Hash as Crypto
import qualified Data.ByteArray.Encoding as Mem
import Web.Scotty
import Web.Scotty.Cookie
import Lucid
import Lucid.Html5
import           Data.Maybe                     ( fromJust
                                                , fromMaybe
                                                )
import qualified Conferer as C
import Conferer.FetchFromConfig.Warp ()
import Control.Monad.Except
import Data.Either.Combinators
import qualified Database.Redis as Redis
import           Data.UUID (UUID)
import qualified Data.UUID as UUID
import System.Random (randomIO)
import qualified Data.Aeson as JSON
import LoginApp (makeLoginApp)

data BackofficePayload =
  BackofficePayload
  { payloadUid :: Int
  , payloadEmail :: Text
  , payloadUsername :: Text
  , payloadFullName :: Text
  , payloadEsRoot :: Bool
  } deriving (Show, Eq, Generic)

instance JSON.ToJSON BackofficePayload
instance JSON.FromJSON BackofficePayload

getUserFromToken :: Redis.Connection -> Request -> IO (Either String BackofficePayload)
getUserFromToken conn req = runExceptT $ do
  (headerName, headerContent) <- liftEither $ maybeToRight "No hay header de cookies" $
    find (\(headerName, headerContent) -> headerName == hCookie) $ requestHeaders req
  (cookieName, cookieContent) <- liftEither $ maybeToRight "No hay cookie de autorization" $
    find (\(cookieName, cookieContent) -> cookieName == "jaimdal")$ parseCookies headerContent
  contenidoDeRedis <- ExceptT $ fmap ((maybeToRight "No estaba la key" =<<) . mapLeft (const "Falló redis")) $ Redis.runRedis conn $ do
    Redis.get cookieContent
  usuario <- ExceptT $ return $ mapLeft (const "Fallo parsear el usuario") $ JSON.eitherDecodeStrict contenidoDeRedis
  liftEither $ Right $ usuario

type BackofficeSignature = ByteString

validateBackofficePayload :: BackofficeSecret -> BackofficeSignature -> BackofficePayload -> Bool
validateBackofficePayload boSecret originalHmac boPayload =
  let hmacExpected = cs $ generateHmacForPayload boSecret boPayload
  in
    hmacExpected == originalHmac

type BackofficeSecret = ByteString

generateHmacForPayload :: BackofficeSecret -> BackofficePayload -> ByteString
generateHmacForPayload boSecret (BackofficePayload unUid email username fullName esRoot) =
  let encodedValue = cs $
        Text.concat
        [ "uid="
        , Text.pack $ show unUid
        , "&email="
        , email
        , "&username="
        , username
        , "&full_name="
        , fullName
        , "&root="
        , case esRoot of
            True -> "true"
            False -> "false"
        ]

  in Mem.convertToBase Mem.Base16 $
     Crypto.hmacGetDigest  $
     Crypto.hmac @ByteString @ByteString @Crypto.SHA256
     boSecret
     encodedValue