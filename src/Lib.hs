{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE StandaloneDeriving #-}
module Lib where

import GHC.Generics
import qualified Conferer as C
import qualified Network.HTTP.Client as Client
import           Network.HTTP.Types
import           Network.HTTP.Types.Header (hCookie, hSetCookie)
import           Network.Wai
import           Network.Wai.Handler.Warp as Warp
import Control.Monad
import Network.HTTP.ReverseProxy
import Web.Cookie
import Data.String.Conversions
import Data.Binary.Builder (toLazyByteString)
import           Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import qualified Data.ByteString.UTF8 as BS
import qualified Data.ByteString.Lazy as LBS
import Data.List (find)
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Lazy  as Text (toStrict)
import qualified Crypto.MAC.HMAC as Crypto
import qualified Crypto.Hash as Crypto
import qualified Data.ByteArray.Encoding as Mem
import Web.Scotty
import Web.Scotty.Cookie
import Lucid
import Lucid.Html5
import Config
import           Data.Maybe                     ( fromJust
                                                , fromMaybe
                                                )
import Control.Monad.Except
import Data.Either.Combinators
import qualified Database.Redis as Redis
import           Data.UUID (UUID)
import qualified Data.UUID as UUID
import System.Random (randomIO)
import qualified Data.Aeson as JSON
import LoginApp (makeLoginApp)
import TokenValidation (getUserFromToken, payloadEsRoot)

mai = do
  manager <- Client.newManager $ Client.defaultManagerSettings { Client.managerConnCount = 500, Client.managerIdleConnectionCount = 500 }
  conf <- C.defaultConfigWithDefaults "patova" []
  appConfig <- C.getFromConfig @AppConfig "" conf
  print appConfig
  putStrLn $ "started proxy on " ++ show (getPort $ appConfigServer $ appConfig) 

  Redis.withCheckedConnect (appConfigRedis appConfig) $ \conn -> do
    loginApp <- makeLoginApp conn appConfig 
    runSettings (appConfigServer appConfig) $ waiProxyTo (coso appConfig conn loginApp) defaultOnExc manager

coso appConfig conn loginApp req =
  if rawPathInfo req == "/__/logout"
    then return $ WPRApplication loginApp
    else 
      getUserFromToken conn req 
      >>= \case
        Left _ ->
          return $ WPRApplication loginApp
        Right user -> do
          let 
            newHeaders = ("X-Jaimdal-Auth", LBS.toStrict $ JSON.encode user) : requestHeaders req
            outgoingRequest = req { requestHeaders = newHeaders }
          if appConfigRootsOnly appConfig && not (payloadEsRoot user)
            then return $ WPRApplication loginApp
            else return $ WPRModifiedRequest outgoingRequest $ 
                    ProxyDest (proxyConfigHost $ appConfigProxy appConfig) (proxyConfigPort $ appConfigProxy appConfig)
