FROM alpine

WORKDIR /app

COPY patova-exe .

ENTRYPOINT ["./patova-exe"]
